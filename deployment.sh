#!/bin/bash
git pull origin master
docker pull alextest321/nostra:latest

echo -e '\e[1m\e[34m\nRestarting service..\e[0m\n'
#stop the application
sudo supervisorctl stop demo-api:*

#make sure close port 8080
sudo fuser -k 8080/tcp

sudo supervisorctl start demo-api:*
echo -e '\n\e[1m\e[34mDeployment successful\e[0m'