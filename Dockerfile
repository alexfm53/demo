# Maven build container 

FROM maven:3.6.3-jdk-8 AS maven_build

COPY pom.xml /tmp/

COPY src /tmp/src/

WORKDIR /tmp/

RUN mvn install

#pull base image

FROM openjdk:8-jdk-alpine

#maintainer 
#expose port 8080
EXPOSE 8080

#default command
CMD java -jar /data/hello-world-0.1.0.jar

#copy hello world to docker image from builder image

COPY --from=maven_build /tmp/target/demo-0.0.1-SNAPSHOT.jar /data/hello-world-0.1.0.jar
